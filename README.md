# README #

This README belongs to an implementation made by me for the selection process of Avenue Code.

####How to compile application?
`mvn clean install`

####How to run automated tests?
`mvn test`

####How to run the application?
`mvn spring-boot:run`

####Database
Exists a file **data.sql** in folder `/src/main/resources` which is load by the Application

####H2 Console
[http://localhost:8080/console](http://localhost:8080/console)

######**JDBC URL  to connect**
 jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE;DATABASE_TO_UPPER=false

######**Credentials**
user: sa


####CRUD Operations (APIs)

- Create Product
[http://localhost:8080/avenuecode/product/create](http://localhost:8080/avenuecode/product/create) **(POST)**

- Update Product
[http://localhost:8080/avenuecode/product/update](http://localhost:8080/avenuecode/product/update) **(POST)**

- Create Image
[http://localhost:8080/avenuecode/image/create](http://localhost:8080/avenuecode/image/create) **(POST)**

- Update Image
[http://localhost:8080/avenuecode/image/update](http://localhost:8080/avenuecode/image/update) **(POST)**

- Get all products excluding relationships (child products, images)
[http://localhost:8080/avenuecode/allProductsWithoutRelationship](http://localhost:8080/avenuecode/allProductsWithoutRelationship) **(GET)**

- Get all products including specified relationships (child product and/or images)
[http://localhost:8080/avenuecode/allProducts](http://localhost:8080/avenuecode/allProducts) **(GET)**

- Same as 3 using specific product identity
[http://localhost:8080/avenuecode/productWithoutRelationship/1](http://localhost:8080/avenuecode/productWithoutRelationship/1) **(GET)**

- Same as 4 using specific product identity
[http://localhost:8080/avenuecode/product/1](http://localhost:8080/avenuecode/product/1) **(GET)**

- Get set of child products for specific product
[http://localhost:8080/avenuecode/childProducts/1](http://localhost:8080/avenuecode/childProducts/1) **(GET)**

- Get set of images for specific product
[http://localhost:8080/avenuecode/imagesByProduct/1](http://localhost:8080/avenuecode/imagesByProduct/1) **(GET)**

- Delete Product
[http://localhost:8080/avenuecode/product/delete/1](http://localhost:8080/avenuecode/product/delete/1) **(DELETE)**

- Delete Image
[http://localhost:8080/avenuecode/image/delete/1](http://localhost:8080/avenuecode/image/delete/1) **(DELETE)**
