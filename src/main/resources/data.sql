--PRODUCTS
INSERT INTO product (id, name, description, parent) VALUES (1, 'Product 1', 'Description 1', null);
INSERT INTO product (id, name, description, parent) VALUES (2, 'Product 2', 'Description 2', 1);
INSERT INTO product (id, name, description, parent) VALUES (3, 'Product 3', 'Description 3', 1);
INSERT INTO product (id, name, description, parent) VALUES (4, 'Product 4', 'Description 4', 2);
INSERT INTO product (id, name, description, parent) VALUES (5, 'Product 5', 'Description 5', 2);
INSERT INTO product (id, name, description, parent) VALUES (6, 'Product 6', 'Description 6', null);
INSERT INTO product (id, name, description, parent) VALUES (7, 'Product 7', 'Description 7', null);
INSERT INTO product (id, name, description, parent) VALUES (8, 'Product 8', 'Description 8', null);
INSERT INTO product (id, name, description, parent) VALUES (9, 'Product 9', 'Description 9', 3);
INSERT INTO product (id, name, description, parent) VALUES (10, 'Product 10', 'Description 10', 4);

--IMAGES
INSERT INTO image (id, type, product) VALUES (1, 'TYPE 1', 1);
INSERT INTO image (id, type, product) VALUES (2, 'TYPE 2', 2);
INSERT INTO image (id, type, product) VALUES (3, 'TYPE 3', 3);
INSERT INTO image (id, type, product) VALUES (4, 'TYPE 4', 4);
INSERT INTO image (id, type, product) VALUES (5, 'TYPE 5', 5);