package com.br.avenuecode.avenuecode.dao;

import com.br.avenuecode.avenuecode.model.Image;

/**
 * @author Samuel Catalano
 */

public interface ImageDAO extends BaseDAO<Image> {
}