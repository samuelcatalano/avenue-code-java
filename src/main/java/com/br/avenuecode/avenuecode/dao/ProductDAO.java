package com.br.avenuecode.avenuecode.dao;

import com.br.avenuecode.avenuecode.model.Product;

import java.util.Set;

/**
 * @author Samuel Catalano
 */

public interface ProductDAO extends BaseDAO<Product> {
	
	Set<Product> getAllWithoutRelationships();
	Product getProductWithoutRelationship(Long id);
}