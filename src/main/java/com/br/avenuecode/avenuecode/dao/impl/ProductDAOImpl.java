package com.br.avenuecode.avenuecode.dao.impl;

import com.br.avenuecode.avenuecode.dao.ProductDAO;
import com.br.avenuecode.avenuecode.model.Product;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Samuel Catalano
 */

@Repository
@Transactional
public class ProductDAOImpl extends BaseDAOImpl<Product> implements ProductDAO{
	
	@Override
	public Set<Product> getAllWithoutRelationships(){
		return new HashSet(defineBeanCriteria().list());
	}
	
	@Override
	public Product getProductWithoutRelationship(final Long id) {
		return (Product) defineBeanCriteria().add(Restrictions.eq("id", id)).uniqueResult();
	}
	
	/**
	 * Define the parameters of bean in database query
	 * @return Product with the specified criteria
	 */
	private Criteria defineBeanCriteria() {
		return getSession().createCriteria(Product.class)
			.setProjection(Projections.projectionList()
			.add(Projections.property("id"), "id")
			.add(Projections.property("name"), "name")
			.add(Projections.property("description"), "description"))
			.setResultTransformer(Transformers.aliasToBean(Product.class));
	}
}