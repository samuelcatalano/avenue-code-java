package com.br.avenuecode.avenuecode.dao.impl;

import com.br.avenuecode.avenuecode.dao.ImageDAO;
import com.br.avenuecode.avenuecode.model.Image;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Samuel Catalano
 */

@Repository
@Transactional
public class ImageDAOImpl extends BaseDAOImpl<Image> implements ImageDAO{
}