package com.br.avenuecode.avenuecode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Samuel Catalano
 */

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
public class ProductDTO implements Serializable {
	
	@Getter
	@Setter
	private Long id;
	
	@Getter
	@Setter
	private String name;
	
	@Getter
	@Setter
	private String description;
	
	@Getter
	@Setter
	private ProductDTO parent;
	
	@Getter
	@Setter
	private Set<ProductDTO> children;
	
	@Getter
	@Setter
	private Set<ImageDTO> images;
}