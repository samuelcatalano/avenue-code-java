package com.br.avenuecode.avenuecode.dto;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
 * @author Samuel Catalano
 */

public class ImageDTO implements Serializable{
	
	@Getter
	@Setter
	private Long id;
	
	@Getter
	@Setter
	private String type;
	
	@Getter
	@Setter
	private ProductDTO product;
}