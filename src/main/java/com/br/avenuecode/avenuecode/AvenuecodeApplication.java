package com.br.avenuecode.avenuecode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Samuel Catalano
 */

@SpringBootApplication
public class AvenuecodeApplication{
	
	public static void main(final String[] args){
		SpringApplication.run(AvenuecodeApplication.class, args);
	}
}