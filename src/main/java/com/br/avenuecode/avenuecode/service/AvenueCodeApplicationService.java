package com.br.avenuecode.avenuecode.service;

import com.br.avenuecode.avenuecode.dao.ImageDAO;
import com.br.avenuecode.avenuecode.dao.ProductDAO;
import com.br.avenuecode.avenuecode.dto.ImageDTO;
import com.br.avenuecode.avenuecode.dto.ProductDTO;
import com.br.avenuecode.avenuecode.model.Image;
import com.br.avenuecode.avenuecode.model.Product;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Samuel Catalano
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/avenuecode")
@Transactional
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AvenueCodeApplicationService {
	
	private final ProductDAO productDAO;
	private final ImageDAO imageDAO;
	private final ModelMapper mapper;
	
	/**
	 * Create a product.
	 * @param product the representation of product
	 * @return Response with HTTP status code
	 */
	@POST
	@RequestMapping("/product/create")
	@Consumes("service/json")
	public Response createProduct(@RequestBody final ProductDTO product) {
		final Product entity = this.mapper.map(product, Product.class);
		this.productDAO.create(entity);
		return Response.status(200).entity(product).build();
	}
	
	/**
	 * Create a image of a product.
	 * @param image the representation of image
	 * @return Response with HTTP status code
	 */
	@POST
	@RequestMapping("/image/create")
	@Consumes("service/json")
	public Response createImage(@RequestBody final ImageDTO image) {
		final Image entity = this.mapper.map(image, Image.class);
		this.imageDAO.create(entity);
		return Response.status(200).entity(image).build();
	}
	
	/**
	 * Update the product.
	 * @param product the representation of product
	 * @return Response with HTTP status code
	 */
	@POST
	@RequestMapping("/product/update")
	@Consumes("service/json")
	public Response updateProduct(@RequestBody final ProductDTO product) {
		final Product entity = this.mapper.map(product, Product.class);
		this.productDAO.update(entity);
		return Response.status(200).entity(product).build();
	}
	
	/**
	 * Update a image.
	 * @param image the representation of image
	 * @return Response with HTTP status code
	 */
	@POST
	@RequestMapping("/image/update")
	@Consumes("service/json")
	public Response updateImage(@RequestBody final ImageDTO image) {
		final Image entity = this.mapper.map(image, Image.class);
		this.imageDAO.update(entity);
		return Response.status(200).entity(image).build();
	}
	
	/**
	 * Get all products with relationships.
	 * @return list of Product
	 */
	@GET
	@RequestMapping("/allProducts")
	@ResponseBody
	public Set<Product> getAllProducts(){
		return new HashSet(this.productDAO.getAll());
	}
	
	/**
	 * Get all products with relationships.
	 * @return list of Product
	 */
	@GET
	@RequestMapping("/allImages")
	@ResponseBody
	public Set<Product> getAllImages(){
		return new HashSet(this.imageDAO.getAll());
	}
	
	/**
	 * Get all products without relationships.
	 * @return list of Product
	 */
	@GET
	@RequestMapping("/allProductsWithoutRelationship")
	public Set<Product> getAllProductsWithoutRelationship(){
		return this.productDAO.getAllWithoutRelationships();
	}
	
	/**
	 * Get a product of a specified id
	 * @param id the id of product
	 * @return Product
	 */
	@GET
	@RequestMapping("/product/{id}")
	public Product getProduct(@PathVariable("id") final Long id){
		return this.productDAO.get(id);
	}
	
	/**
	 * Get an image of an specified id
	 * @param id the id of image
	 * @return Image
	 */
	@GET
	@RequestMapping("/image/{id}")
	public Image getImage(@PathVariable("id") final Long id){
		return this.imageDAO.get(id);
	}
	
	/**
	 * Get a product of a specified id, without its relationships
	 * @param id the id of product
	 * @return Product
	 */
	@GET
	@RequestMapping("/productWithoutRelationship/{id}")
	public Product getProductWithoutRelationship(@PathVariable("id") final Long id){
		return this.productDAO.getProductWithoutRelationship(id);
	}
	
	/**
	 * Get all children from a specific product.
	 * @param id the id of product
	 * @return list of Product
	 */
	@GET
	@RequestMapping("/childProducts/{id}")
	public Set<Product> getChildProducts(@PathVariable("id") final Long id) {
		return this.productDAO.get(id).getChildren();
	}
	
	/**
	 * Get all images from a specific product.
	 * @param id the id of product
	 * @return list of Image
	 */
	@GET
	@RequestMapping("/imagesByProduct/{id}")
	public Set<Image> getImagesByProduct(@PathVariable("id") final Long id) {
		return this.productDAO.get(id).getImages();
	}
	
	/**
	 * Delete product with a specific id and all relationships.
	 * @param id the id of product
	 * @return Response with HTTP status code
	 */
	@DELETE
	@RequestMapping("/product/delete/{id}")
	public Response deleteProduct(@PathVariable("id") final Long id) {
		this.productDAO.deleteById(id);
		return Response.status(200).build();
	}
	
	/**
	 * Delete an image with a specific id.
	 * @param id the id of image
	 * @return Response with HTTP status code
	 */
	@DELETE
	@RequestMapping("/image/delete/{id}")
	public Response deleteImage(@PathVariable("id") final Long id) {
		this.imageDAO.deleteById(id);
		return Response.status(200).entity(id).build();
	}
}