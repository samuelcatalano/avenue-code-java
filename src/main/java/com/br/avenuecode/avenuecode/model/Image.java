package com.br.avenuecode.avenuecode.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Samuel Catalano
 */

@Entity
@Table(name = "image")
public class Image implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	@Getter
	@Setter
	private Long id;
	
	@Column(name = "type", nullable = false, length = Integer.MAX_VALUE)
	@Getter
	@Setter
	private String type;
	
	@JoinColumn(name = "product", referencedColumnName = "id")
	@ManyToOne(optional = false)
	@Getter
	@Setter
	private Product product;
}