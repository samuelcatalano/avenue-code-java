package com.br.avenuecode.avenuecode;

import com.br.avenuecode.avenuecode.dao.ProductDAO;
import com.br.avenuecode.avenuecode.model.Image;
import com.br.avenuecode.avenuecode.model.Product;
import com.br.avenuecode.avenuecode.service.AvenueCodeApplicationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Samuel Catalano
 */

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@EntityScan({"com.br.avenuecode.avenuecode.model"})
@SpringBootTest
public class AvenuecodeApplicationTests {
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private AvenueCodeApplicationService applicationService;
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private ProductDAO productDAO;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
	@Test
	public void testContexLoads() throws Exception {
		assertThat(this.applicationService).isNotNull();
	}
	
	@Transactional
	@Test
	public void testGetAllProducts() throws Exception{
		this.mockMvc.perform(get("/avenuecode/allProducts")).andDo(print())
			.andExpect(status()
			.isOk());
	}
	
	@Transactional
	@Test
	public void testGetAllProductsWithoutRelationship() throws Exception {
		this.mockMvc.perform(get("/avenuecode/allProductsWithoutRelationship")).andDo(print())
			.andExpect(status()
			.isOk());
	}
	
	@Transactional
	@Test
	public void testGetProduct() throws Exception {
		this.mockMvc.perform(get("/avenuecode/product/1")).andDo(print())
			.andExpect(status()
			.isOk());
	}
	
	@Transactional
	@Test
	public void testGetImage() throws Exception {
		this.mockMvc.perform(get("/avenuecode/image/1")).andDo(print())
			.andExpect(status()
			.isOk());
	}
	
	@Transactional
	@Test
	public void testGetProductWithoutRelationship() throws Exception {
		this.mockMvc.perform(get("/avenuecode/productWithoutRelationship/1")).andDo(print())
			.andExpect(status()
			.isOk());
	}
	
	@Transactional
	@Test
	public void testGetImagesFromProduct() throws Exception {
		this.mockMvc.perform(get("/avenuecode/imagesByProduct/1")).andDo(print())
			.andExpect(status()
			.isOk());
	}
	
	@Transactional
	@Test
	public void testSaveProduct() throws Exception {
		final Product product = new Product();
		product.setName("New Product");
		product.setDescription("New Description");
		
		this.mockMvc.perform(post("/avenuecode/product/create")
			.contentType(MediaType.APPLICATION_JSON)
			.content(this.objectMapper.writeValueAsString(product)))
			.andExpect(status().isOk());
	}
	
	@Transactional
	@Test
	public void testSaveImage() throws Exception {
		final Product product = this.productDAO.get(1L);
		final Image image = new Image();
		image.setType("TYPE NEW");
		image.setProduct(product);
		
		this.mockMvc.perform(post("/avenuecode/image/create")
			.contentType(MediaType.APPLICATION_JSON)
			.content(this.objectMapper.writeValueAsString(image)))
			.andExpect(status().isOk());
	}
	
	@Transactional
	@Test
	public void testSaveProductWithParent() throws Exception {
		final Product product = new Product();
		product.setName("New Product");
		product.setDescription("New Description");
		product.setParent(product);

		this.mockMvc.perform(post("/avenuecode/product/create")
			.contentType(MediaType.APPLICATION_JSON)
			.content(this.objectMapper.writeValueAsString(product)))
			.andExpect(status().isOk());
	}
	
	@Transactional
	@Test
	public void testUpdateProduct() throws Exception {
		final Product product = new Product();
		product.setId(2L);
		product.setName("New Product TEST");
		product.setDescription("New Description TEST");
		
		this.mockMvc.perform(post("/avenuecode/product/update")
			.contentType(MediaType.APPLICATION_JSON)
			.content(this.objectMapper.writeValueAsString(product)))
			.andExpect(status().isOk());
	}
	
	@Transactional
	@Test
	public void testUpdateImage() throws Exception {
		final Product product = this.productDAO.get(1L);
		final Image image = new Image();
		image.setId(2L);
		image.setType("TYPE NEW UPDATED");
		image.setProduct(product);
		
		this.mockMvc.perform(post("/avenuecode/image/create")
			.contentType(MediaType.APPLICATION_JSON)
			.content(this.objectMapper.writeValueAsString(image)))
			.andExpect(status().isOk());
	}
	
	@Transactional
	@Test
	public void testDeleteImage() throws Exception {
		this.mockMvc.perform(get("/avenuecode/image/delete/1")).andDo(print())
			.andExpect(status()
			.isOk());
	}
	
	@Transactional
	@Test
	public void testDeleteProduct() throws Exception {
		this.mockMvc.perform(get("/avenuecode/product/delete/1")).andDo(print())
			.andExpect(status()
			.isOk());
	}
	
	@Transactional
	@Test
	public void testGetAllProductsInWrongPath() throws Exception{
		this.mockMvc.perform(get("/avenuecode/wrongPathAllProducts")).andDo(print())
			.andExpect(status()
			.isNotFound());
	}
	
	@Transactional
	@Test
	public void testExpectedSizeFromAllProducts() throws Exception {
		this.mockMvc.perform(get("/avenuecode/allProducts")).andDo(print())
			.andExpect(jsonPath("$", hasSize(10)));
	}
	
	@Transactional
	@Test
	public void testAllProductsCollectionNotEmpty() throws Exception{
		this.mockMvc.perform(get("/avenuecode/allProducts")).andDo(print())
			.andExpect(jsonPath("$").isNotEmpty());
	}
	
	@Transactional
	@Test
	public void testAllImagesCollectionNotEmpty() throws Exception{
		this.mockMvc.perform(get("/avenuecode/allImages")).andDo(print())
			.andExpect(jsonPath("$").isNotEmpty());
	}
	
	@Transactional
	@Test
	public void testGetImagesFromProductInWrongPath() throws Exception {
		this.mockMvc.perform(get("/avenuecode/byProduct/1")).andDo(print())
			.andExpect(status()
			.isNotFound());
	}
	
	@Transactional
	@Test
	public void testGetProductWithoutIdentify() throws Exception {
		this.mockMvc.perform(get("/avenuecode/product/")).andDo(print())
			.andExpect(status()
			.isNotFound());
	}
	
	@Transactional
	@Test
	public void testGetImageWithoutIdentify() throws Exception {
		this.mockMvc.perform(get("/avenuecode/image/")).andDo(print())
			.andExpect(status()
			.isNotFound());
	}
	
	@Transactional
	@Test
	public void testGetProductStrangeId() throws Exception {
		this.mockMvc.perform(get("/avenuecode/product/@#$%ˆ")).andDo(print())
			.andExpect(status()
			.isBadRequest());
	}
}